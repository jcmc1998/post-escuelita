package com.somospnt.builder;

import com.somospnt.domain.Entrevista;
import com.somospnt.test.builder.AbstractPersistenceBuilder;
import java.time.LocalDateTime;

public class EntrevistaBuilder extends AbstractPersistenceBuilder<Entrevista> {

    private EntrevistaBuilder() {
        instance = new Entrevista();
    }
    
    public static EntrevistaBuilder entrevistaValidaDeUsuario(long entrevistadorId) {
        EntrevistaBuilder builder = new EntrevistaBuilder();
        builder.instance.setNombreEntrevistado("Pepe");
        builder.instance.setFecha(LocalDateTime.now());
        builder.instance.setRealizada(false);
        builder.instance.setIdEntrevistador(entrevistadorId);
        return builder;
    }
    
    
   

}
