package com.somospnt.service.impl;

import com.somospnt.builder.EntrevistaBuilder;
import com.somospnt.domain.Entrevista;
import com.somospnt.repository.EntrevistaRepository;
import com.somospnt.service.EntrevistaService;
import java.util.List;
import java.util.NoSuchElementException;
import javax.persistence.EntityManager;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class EntrevistaServiceImplTest {

    @Autowired
    private EntrevistaService entrevistaService;

    @Autowired
    private EntrevistaRepository entrevistaRepository;

    @Autowired
    private EntityManager entityManager;

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void buscarTodas_conUsuarioNoLogueado_lanzaExcepcion() {
        entrevistaService.buscarTodas();
    }

    @Test
    @WithMockUser(username = "juan")
    public void buscarTodas_conEntrevistasExistentes_retornaTodasLasEntrevistas() {
        EntrevistaBuilder.entrevistaValidaDeUsuario(1).build(entityManager);
        EntrevistaBuilder.entrevistaValidaDeUsuario(2).build(entityManager);

        List<Entrevista> entrevistas = entrevistaService.buscarTodas();

        assertFalse(entrevistas.isEmpty());

        for (Entrevista entrevista : entrevistas) {
            assertEquals(1, entrevista.getIdEntrevistador());
        }
    }

    @Test
    public void guardar_conCamposValidos_retornaEntrevistaGuardada() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();

        Entrevista entrevistaGuardada = entrevistaService.guardar(entrevista);

        assertNotNull(entrevistaGuardada.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conIdExistente_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setId(1L);

        entrevistaService.guardar(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conFechaNull_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setFecha(null);

        entrevistaService.guardar(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conNombreNull_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setNombreEntrevistado(null);

        entrevistaService.guardar(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conNombreVacio_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setNombreEntrevistado("");

        entrevistaService.guardar(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conNumerosEnNombre_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setNombreEntrevistado("julio213213");

        entrevistaService.guardar(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conEspaciosEnNombre_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setNombreEntrevistado("J  ulio");

        entrevistaService.guardar(entrevista);
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conNumerosEnApellido_lanzaExcepcion() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setApellidoEntrevistado("Ma123123c");

        entrevistaService.guardar(entrevista);
    }

    @Test
    public void guardar_conApellidoNull_retornaEntrevistaGuardada() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setApellidoEntrevistado(null);

        Entrevista entrevistaGuardada = entrevistaService.guardar(entrevista);

        assertNotNull(entrevistaGuardada.getId());
    }

    @Test
    public void guardar_conApellidoVacio_retornaEntrevistaGuardada() {
        Entrevista entrevista = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build();
        entrevista.setApellidoEntrevistado(" ");

        Entrevista entrevistaGuardada = entrevistaService.guardar(entrevista);

        assertNotNull(entrevistaGuardada.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void guardar_conEntrevistaNull_lanzaExcepcion() {
        entrevistaService.guardar(null);
    }

    @Test(expected = NoSuchElementException.class)
    public void eliminarPorId_conIdInexistente_lanzaExcepcion() {
        Long idInexistente = -1L;

        entrevistaService.eliminarPorId(idInexistente);
    }

    @Test
    public void eliminarPorId_conIdExistente_entrevistaEliminada() {
        Entrevista entrevistaGuardada = EntrevistaBuilder.entrevistaValidaDeUsuario(1).build(entityManager);
        Long entrevistaId = entrevistaGuardada.getId();

        entrevistaService.eliminarPorId(entrevistaId);
        boolean esExistente = entrevistaRepository.existsById(entrevistaId);

        assertFalse(esExistente);
    }

}
