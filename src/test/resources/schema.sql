set mode MYSQL;

DROP TABLE IF EXISTS entrevista;
DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS entrevistador;

CREATE TABLE entrevistador ( 
    id INT NOT NULL AUTO_INCREMENT,
    usuario VARCHAR(20) NOT NULL, 
    password VARCHAR(200) NOT NULL, 
    PRIMARY KEY (id), 
    UNIQUE (usuario)
);

CREATE TABLE entrevista (
   id INT NOT NULL AUTO_INCREMENT,
   nombre_entrevistado VARCHAR(45) NOT NULL,
   apellido_entrevistado VARCHAR(45) NOT NULL,
   fecha DATETIME NOT NULL,
   realizada TINYINT NOT NULL,
   id_entrevistador INT NOT NULL,
   PRIMARY KEY (id),
   FOREIGN KEY (id_entrevistador) REFERENCES entrevistador(id)
);

CREATE TABLE authorities (
    username VARCHAR(50) NOT NULL UNIQUE,
    authority VARCHAR(50) NOT NULL, 
    FOREIGN KEY (username) REFERENCES entrevistador(usuario)
);

