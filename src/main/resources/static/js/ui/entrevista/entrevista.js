app.ui.entrevista = (function () {

    function inicializar() {
        bindearEventosABotones();
    }

    function bindearEventosABotones() {
        $('[data-toggle="tooltip"]').tooltip();
        $('.pnt-boton-eliminar').on('click', mostrarModalEliminar);
    }

    function mostrarModalEliminar() {
        var eliminarId = $(this).data('id');
        $('#eliminarModal').modal('show');
        $('#boton-eliminar-modal').on('click',function (){
            app.service.entrevista.eliminarPorId(eliminarId)
        });
    }

    return {
        inicializar: inicializar
    };

})();

$(document).ready(function () {
    app.ui.entrevista.inicializar();
});

