app.service.entrevista = (function () {

    var urlHome = "/entrevistas";
    var urlService = "api/entrevistas/";

    function eliminarPorId(id) {
        return $.ajax({
            url: urlService + id,
            type: 'DELETE',
            success: function () {
                window.location.href = urlHome;

            }
        });
    }

    return {
        eliminarPorId: eliminarPorId
    };
})();