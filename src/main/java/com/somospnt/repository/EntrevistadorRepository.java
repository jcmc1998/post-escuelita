package com.somospnt.repository;

import com.somospnt.domain.Entrevistador;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrevistadorRepository extends JpaRepository<Entrevistador, String> {

    Entrevistador findByUsuario(String usuario);
}
