package com.somospnt.repository;

import com.somospnt.domain.Entrevista;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrevistaRepository extends JpaRepository<Entrevista, Long> {

    List<Entrevista> findByIdEntrevistador(long id);

}
