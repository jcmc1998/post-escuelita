package com.somospnt.controller.rest;

import com.somospnt.domain.Entrevista;
import com.somospnt.service.EntrevistaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping("/api/entrevistas")
public class EntrevistaRestController {

    @Autowired
    private EntrevistaService entrevistaService;

    @GetMapping("/")
    public List<Entrevista> buscarTodas() {
        return entrevistaService.buscarTodas();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Entrevista guardar(@RequestBody Entrevista entrevista) {
        return entrevistaService.guardar(entrevista);
    }

    @DeleteMapping("/{entrevistaId}")
    public void eliminarPorId(@PathVariable Long entrevistaId) {
        entrevistaService.eliminarPorId(entrevistaId);
    }
}
