package com.somospnt.controller;

import com.somospnt.domain.Entrevista;
import com.somospnt.service.EntrevistaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EntrevistaController {

    @Autowired
    private EntrevistaService entrevistaService;

    @GetMapping("/")
    public String home(Model model) {
        List<Entrevista> entrevistas = entrevistaService.buscarTodas();
        model.addAttribute("entrevistas", entrevistas);
        return "entrevistas";
    }

    @RequestMapping("/formulario")
    public String formularioEntrevista() {
        return "formulario-entrevista";
    }

    @PostMapping("/guardar-entrevista")
    public String guardarEntrevista(Entrevista entrevista, Model model) {
        try {
            entrevistaService.guardar(entrevista);
        } catch (IllegalArgumentException ex) {
            model.addAttribute("mensajeError", ex.getMessage());
            return "formulario-entrevista";
        }
        return "redirect:/";
    }

    @ExceptionHandler(Exception.class)
    public String errorServidor() {
        return "error-servidor";
    }

}
