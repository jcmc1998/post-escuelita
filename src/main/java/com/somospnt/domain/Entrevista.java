package com.somospnt.domain;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
public class Entrevista {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private LocalDateTime fecha;
    private String nombreEntrevistado;
    private String apellidoEntrevistado;
    private boolean realizada;
    private long idEntrevistador;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getNombreEntrevistado() {
        return nombreEntrevistado;
    }

    public void setNombreEntrevistado(String nombreEntrevistado) {
        this.nombreEntrevistado = nombreEntrevistado;
    }

    public String getApellidoEntrevistado() {
        return apellidoEntrevistado;
    }

    public void setApellidoEntrevistado(String apellidoEntrevistado) {
        this.apellidoEntrevistado = apellidoEntrevistado;
    }

    public boolean isRealizada() {
        return realizada;
    }

    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }

    public long getIdEntrevistador() {
        return idEntrevistador;
    }

    public void setIdEntrevistador(long idEntrevistador) {
        this.idEntrevistador = idEntrevistador;
    }

}
