package com.somospnt.service;

import com.somospnt.domain.Entrevista;
import java.util.List;

public interface EntrevistaService {

    List<Entrevista> buscarTodas();

    Entrevista guardar(Entrevista entrevista);

    void eliminarPorId(long id);

}
