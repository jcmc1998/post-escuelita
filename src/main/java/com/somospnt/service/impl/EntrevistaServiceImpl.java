package com.somospnt.service.impl;

import com.somospnt.domain.Entrevista;
import com.somospnt.domain.Entrevistador;
import com.somospnt.repository.EntrevistaRepository;
import com.somospnt.repository.EntrevistadorRepository;
import com.somospnt.service.EntrevistaService;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Transactional
public class EntrevistaServiceImpl implements EntrevistaService {

    @Autowired
    private EntrevistaRepository entrevistaRepository;

    @Autowired
    private EntrevistadorRepository entrevistadorRepository;

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Entrevista> buscarTodas() {
        long entrevistadorLogueadoId = buscarEntrevistadorId();

        return entrevistaRepository.findByIdEntrevistador(entrevistadorLogueadoId);
    }

    @Override
    public Entrevista guardar(Entrevista entrevista) {
        Assert.notNull(entrevista, "Entrevista no puede ser null");
        Assert.isNull(entrevista.getId(), "Entrevista ya existe");
        Assert.notNull(entrevista.getNombreEntrevistado(), "Nombre del entrevistado no puede ser nulo");
        Assert.isTrue(noEsVacio(entrevista.getNombreEntrevistado()), "Nombre del entrevistado no puede estar vacío");
        Assert.isTrue(noContieneNumerosNiEspacios(entrevista.getNombreEntrevistado()), "No pueden haber números ni espacios en el nombre");
        Assert.isTrue(noContieneNumeros(entrevista.getApellidoEntrevistado()), "No pueden haber números en el apellido");
        Assert.notNull(entrevista.getFecha(), "Fecha no puede estar vacia");
        entrevista.setIdEntrevistador(buscarEntrevistadorId());

        return entrevistaRepository.save(entrevista);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public void eliminarPorId(long id) throws NoSuchElementException {
        try {
            entrevistaRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NoSuchElementException("Entrevista no existe");
        }
    }

    private boolean noContieneNumerosNiEspacios(String string) {
        return !string.matches(".*[0-9 ]+.*");
    }

    private boolean noContieneNumeros(String string) {
        if (string == null) {
            return true;
        }
        return !string.matches(".*\\d.*");
    }

    private boolean noEsVacio(String string) {
        return !string.equals("");
    }

    private long buscarEntrevistadorId() {
        Authentication usuarioLogueado = SecurityContextHolder.getContext().getAuthentication();
        String entrevistadorNombre = usuarioLogueado.getName();
        Entrevistador entrevistador = entrevistadorRepository.findByUsuario(entrevistadorNombre);
        
        return entrevistador.getId();
    }

}
