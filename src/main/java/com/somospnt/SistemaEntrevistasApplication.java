package com.somospnt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaEntrevistasApplication {

    public static void main(String[] args) {
        SpringApplication.run(SistemaEntrevistasApplication.class, args);
    }

}
