CREATE SCHEMA entrevista;

CREATE TABLE entrevista (
   id INT NOT NULL AUTO_INCREMENT,
   nombre_entrevistado VARCHAR(45) NOT NULL,
   apellido_entrevistado VARCHAR(45) NOT NULL,
   fecha DATETIME NOT NULL,
   realizada TINYINT NOT NULL,
  PRIMARY KEY (id));

INSERT INTO `entrevista` (`id`, `nombre_entrevistado`, `apellido_entrevistado`, `fecha`, `realizada`) VALUES ('1', 'Julio', 'Machillanda', '2019-03-01 15:00:00', '1');