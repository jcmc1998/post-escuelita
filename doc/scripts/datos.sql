INSERT INTO entrevistador
(id,    usuario,     password) VALUES 
('1',   'juan',      '$2a$10$Eg.yjJyjouUkimPgaMQyOeW2FsH.SObhhbAwuT2uyLKXRam5Z88Cy'),
('2',   'leo',       '$2a$10$tPpSRmagy/PEg1xV1AGh7ukljHWVe/1meALmPelhlLyLcXo364.Zy');

INSERT INTO entrevista 
(id,    nombre_entrevistado,    apellido_entrevistado,  fecha,                  realizada,  id_entrevistador) VALUES 
('1',   'Julio',                'Machillanda',          '2019-03-01 15:00:00',  '1',        '1'),
('2',   'Cesar',                'Zim',                  '2019-03-02 15:00:00',  '0',        '2'),
('3',   'Alessandro',           'Gomez',                '2019-03-04 15:00:00',  '0',        '2'),
('4',   'Daniel',               'Perez',                '2019-03-05 15:00:00',  '1',        '1'),
('5',   'Jose',                 'Rodriguez',            '2019-03-12 15:00:00',  '0',        '1');

INSERT INTO authorities
(username,  authority) VALUES
('juan','USER'),
('leo','USER');

